include .env

upload:
	platformio run --target upload --target monitor --environment wemos_d1_mini32

compose:
	docker compose up

watcher-toggle:
	curl -L -X POST 'http://localhost:1880/watcher/enable' -H 'Content-Type: application/json' --data-raw '{}'

