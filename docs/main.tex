\documentclass{article}

\usepackage[utf8]{inputenc}
% \usepackage[italian]{babel}
\usepackage{graphicx}
\usepackage{float}
\usepackage{biblatex}
\usepackage{hyperref}
\usepackage{listings}

\addbibresource{./bibliography/bibliography.bib}
\graphicspath{./images/}
\lstset{
    breaklines=true
}

\title{Progetto IoT: Watcher}
\author{Matteo Faraci}
\date{Date}

\begin{document}

\maketitle

\tableofcontents
\newpage

\section{Introduzione}
Ad oggi i sistemi IoT sono molto diffusi: dalle case delle persone alle industrie.
In un mondo sempre più connesso e eterogeneo, si necessitano di dispositivi che si possano adattare all'ambiente circostante e che riescano a comunicare.
L'eterogeneità è uno dei fattori che aggiunge complessità ma fortunamente esistono diverse tecnologie che forniscono degli standard, come la comunicazione.
Watcher è un progetto universitario che tenta di utilizzare alcuni di questi strumenti ed è stato progettato per potersi integrare. 
L'obiettivo che si pone è quello del monitoraggio di un oggetto notificando l'utente se questo si muove senza autorizzazione.
In particolare si tratta di un sistema di allarme, che fa uso di una board ESP32 come componente centrale, utilizza MQTT per la comunicazione e sincronizzazione e PlatformIO come framework.
Inizialmente è stata impiegata un'architettura client-server dove il client è l'utente che vuole monitorare un aggetto e il server la board stessa.
Questa soluzione è stata successivamente scartata perchè il sistema, funzionando a batteria, ha delle fasi di ibernazione che per loro natura, disattivano il dispositivo, spegnendo il server e impedendo la comunicazione. Per questo motivo si è passati ad un paradigma publisher-subscriber. 

\section{Componenti}
Come mostrato dalla Figura~\ref{fig:project_circuit} il progetto si compone di:
\begin{itemize}
  \item Board ESP32: è cuore dell'applicazione, dove viene gestito lo stato e il funzionamento del sistema 
  \item Buzzer: Sistema sonoro di allarme 
  \item Led RGB: Led di notifica dello stato corrente, così che l'utente possa avere un'idea dello stato dell'applicazione
  \item Switch in mercurio: utilizzato per capire se l'oggetto monitorato si è mosso
\end{itemize}
In particolare, lo Switch in mercurio è la componente responsabile del monitoraggio. 
All'interno del vetro, c'è una piccola quantità di mercurio che si muove e se si inclina chiude il circuito, emettendo un segnale. 
Se posizionato su una scatola che non abbia il coperchio removibile, rimane spento, ma se la si prova ad aprire si aziona.
Questo rende il sistema portabile, perchè basta posizionare il sensore su una qualsiasi scatola, per farlo funzionare.

Una soluzione alternativa potrebbe essere implementata con un sensore di rilevamento di oggetti.
\begin{figure}[H]
  \begin{center}
    \includegraphics[width=0.95\textwidth]{./images/project_circuit.jpg}
  \end{center}
  \caption{Progetto con circuiteria}
  \label{fig:project_circuit}
\end{figure}

\section{Funzionamento}
Per il corretto funzionamento, il sistema necessita di una connessione ad internet che viene fornita tramite WiFi.
La configurazione viene fatta tramite variabili d'ambiente, così che i segreti come la password del WiFi non siano scritti nel codice.
Il funzionamento è stato pensato come una macchina a stati finiti. 
In ogni istante il sistema si troverà in un determinato stato e in base agli stimoli reagirà di conseguenza.
In particolare l'applicazione può assumere i seguenti stati (Figura~\ref{fig:state_diagram}):
\begin{itemize}
  \item Initial: Inizialmente il sistema si trova nello stato iniziale ovvero quando non è ancora inizializzato
  \item Idle: La transizione a questo stato avviene secondo le seguenti condizioni; connessione al WiFi e MQTT, disattivazione dell'allarme e disattivazione del monitoraggio
  \item Armed: in questo stato il sistema è armato e sta monitorando un oggetto. Nel momento in cui rileva un movimento entra nello stato Alarm
  \item Alarm: in questo stato il sistema notifica l'utente del movimento dell'oggetto monitorato tramite un suono emesso dal Buzzer e messaggio su topic MQTT
  \item Error: in questo stato il sistema è in errore per un qualsiasi motivo come la mancata connessione al WiFi oppure al broker MQTT
\end{itemize}
Ogni cambiamento di stato ha associato una combinazione di colori che modifica il led RGB, in modo da notificare l'utente dello stato del sistema.
\begin{figure}[H]
  \begin{center}
    \includegraphics[width=0.50\textwidth]{./images/state_diagram.png}
  \end{center}
  \caption{Funzionamento dello stato}
  \label{fig:state_diagram}
\end{figure}


\subsection{Comunicazione e gestione dell'energia}
Watcher è un sistema che è stato progettato per essere alimentato a batteria, quindi è stata adottata una tecnica di risparmio energetico: il deep sleep.
Il sistema, una volta addormentato, può essere risvegliato in modi diversi; in questo caso è stato scelto il timeout perchè di semplice implementazione e raggiungeva l'obiettivo.
Questa scelta ha influenzato il protocollo di comunicazione. 
Inizialmente si è pensato ad un webserver, ma ci si è resi conto che nelle fasi di sleep, la comunicazione tra client e server non poteva funzionare.
Siccome il server eseguiva direttamente sulla board, durante l'ibernazione, questo si disattivava, impedendo la comunicazione tra client e server.

Si è quindi cercato un protocollo che potesse essere utilizzato come una sorta di coda: il messaggio di attivazione del monitoraggio doveva essere salvato e successivamente ricevuto al risveglio di Watcher.
Fortunamente esiste un protocollo che può comportarsi in questo modo: MQTT.
MQTT è un protocollo che funziona tramite paradigma publisher-subscriber e aggiunge una componente intermedia in grado di salvare i messaggi: il broker.
Questo paradigma permette al client di inviare il messaggio di attivazione, che viene salvato nel broker, Watcher in modo asincrono (rispetto all'invio) si connette e lo scarica attivando il monitoraggio. 
In questo modo anche se il comando di attivazione viene lanciato quando Watcher è addormentato, una volta che si risveglierà, troverà il messaggio cambiando lo stato.
Normalmente il broker ignora i messaggi che non hanno subscribers ma grazie al flag `retain`, questo non succede.
E' importante ricordare che è necessario `pulire` i messaggi tramite un messaggio vuoto con `ratain` a true, così da evitare comportamenti scorretti.
Si applica la stessa procedura anche durante le fasi di spegnimento.

\begin{figure}[H]
  \begin{center}
    \includegraphics[width=0.95\textwidth]{./images/loop_diagram.png}
  \end{center}
  \caption{Funzionamento generale del sistema}
  \label{fig:loop_diagram}
\end{figure}

Seguendo lo schema illustrato nella Figura~\ref{fig:loop_diagram}, la sequenza di esecuzione è la seguente:
Il sistema si avvia, successivamente viene impostato lo stato (salvato in memoria se dopo risveglio) e viene verificato se c'è un messaggio.
Se lo trova allora si arma ed elimina il messaggio per evitare che al risveglio successivo si disarmi.

\begin{figure}[H]
  \begin{center}
    \includegraphics[width=0.95\textwidth]{./images/deep_sleep.png}
  \end{center}
  \caption{Preparazione e successiva ibernazione}
  \label{fig:deep-sleep}
\end{figure}

\subsection{Salvataggio dello stato}
Ogni volta che il sistema si risveglia, è come se si riaccendesse, senza che abbia la conoscenza di come fosse configurato precedentemente. 
Si è quindi pensato ad una soluzione che salvasse lo stato e fortunamente è di facile implementazione. 
Basta annotare la variabile di stato globale con `RTC\_DATA\_ATTR`. 
Questa annotazione permette alla variabile di essere salvata nella regione della RAM che persiste anche dopo il risveglio, così che lo stato possa essere conservato.

\begin{figure}[H]
  \begin{center}
    \includegraphics[width=0.95\textwidth]{./images/save_state.png}
  \end{center}
  \caption{Salvataggio dello stato}
  \label{fig:saving-state}
\end{figure}

\section{Integrazione con Node-RED}
Siccome MQTT è un protocollo molto conosciuto in ambito IoT, è possibile integrarlo con altri sistemi come Node-RED.
Node-RED è un strumento di programmazione che permette la realizzazione di diversi `flow` tramite il collegamento di sistemi, sia che siano API o servizi online.
Come da Figura~\ref{fig:node_red_implementation} si è deciso di fornire una dimostrazione di integrazione.
Il flow si divide in due rami, uno che riceve le notifiche di allarme e uno che attiva o disattiva Watcher.

Grazie a Node-RED è possibile attivare e disattivare il monitoraggio tramite richiesta HTTP POST, infatti lo stesso Node-RED esporrà un endpoint API che potrà essere chiamato tramite

\begin{figure}[H]
  \begin{center}
    \includegraphics[width=0.95\textwidth]{./images/watcher-endpoint.png}
  \end{center}
  \caption{Chiamata dell'endpoint HTTP su Node-RED}
  \label{fig:watcher-endpoint}
\end{figure}
Questa chiamata ha come conseguenza l'invio di un messaggio tramite MQTT, da Node-RED, al topic `watcher/enabled`, dove Watcher è iscritto, attivando o disattivando il monitoraggio.
Il flow ha anche un blocco di risposta HTTP così che il client non rimanga in attesa.

L'altro flow utilizza un blocco MQTT che si iscrive al topic `watcher/alarm` dove Watcher invierà messaggi nel caso in cui rilevi un cambiamento.
Dalle interafaccie di debug si possono vedere i messaggi.

\begin{figure}[H]
  \begin{center}
    \includegraphics[width=0.95\textwidth]{./images/node-red.png}
  \end{center}
  \caption{Integrazione tramite Node-RED}
  \label{fig:node_red_implementation}
\end{figure}

\section{Conclusioni}
Il sistema ottenuto, nonostante sia molto semplice è integrabile in diversi scenari perchè fa uso di tecnologie diffuse e molto conosciute.
La scelta del protocollo di comunicazione è stata influenzata dalla decisione di progettare un sistema che funzioni a betteria. 
Sebbene questa scelta abbia reso più complessa l'implementazione, grazie alle funzionalità fornite dalla board e dal framework di proggrammazione, l'obiettivo è stato raggiunto.
Sia le ingregrazioni che le funzionalità possono essere espanse e migliorate, dando vita a scenari più complessi mantenendo la stessa logica di base.


\end{document}

