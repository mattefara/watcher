#include "pins.h"
#include <Arduino.h>
#include <mqtt.h>
#include <notes.h>
#include <state.h>
#include <wifi.h>

#define uS_TO_S_FACTOR                                                         \
  1000000               /* Conversion factor for micro seconds to seconds */
#define TIME_TO_SLEEP 2 /* Time ESP32 will go to sleep (in seconds) */

void init_pins() {
  pinMode(LED_RED, OUTPUT);
  pinMode(LED_GREEN, OUTPUT);
  pinMode(LED_BLUE, OUTPUT);
  pinMode(MOTION_SENSOR, INPUT);
}

void setup() {
  Serial.begin(9600);
  esp_sleep_enable_timer_wakeup(TIME_TO_SLEEP * uS_TO_S_FACTOR);

  init_pins();

  Serial.println("Attempting connection to: " + String(WIFI_SSID));
  IPAddress addr = wifi_connect(LED_RED, LED_GREEN, LED_BLUE);
  update_led_based_on_state(LED_RED, LED_GREEN, LED_BLUE);

  mqtt_initialize();
  mqtt_subscribe();

  State state = get_current_state();
  if (state == State::INITIAL) {
    update_state(State::IDLE);
  } else {
    update_state(get_current_state());
  }
}

int count = 0;

void loop() {

  mqtt_loop();
  if (is_armed() && digitalRead(MOTION_SENSOR) == LOW) {
    update_state(State::ALARM);
  }

  if (get_current_state() == State::ALARM) {
    ring_alarm();
    mqtt_publish(WATCHER_ALARM, "alarm");
  }

  update_led_based_on_state(LED_RED, LED_GREEN, LED_BLUE);

  if (++count % 5 == 0) {
    Serial.println("Going to sleep");
    mqtt_disconnect();
    Serial.flush();
    esp_deep_sleep_start();
  }

  delay(500);
}
