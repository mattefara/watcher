#include "mqtt.h"
#include "state.h"
#include <Arduino.h>
#include <PubSubClient.h>
#include <WiFi.h>

WiFiClient wifiClient;
PubSubClient client(wifiClient);

void callback(char *topic, byte *payload, unsigned int length) {
  if (length == 0) {
    return;
  }

  Serial.println("Received message from MQTT, changing state");

  if (String(topic) == String(WATCHER_ENABLED)) {
    State state = get_current_state();
    if (state == State::ARMED || state == State::ALARM) {
      update_state(State::IDLE);
    }
    if (state == State::IDLE) {
      update_state(State::ARMED);
    }
  }

  // Clean up persistent message
  client.publish(WATCHER_ENABLED, "", true);
}

bool mqtt_check() { return client.connect("Watcher"); }

void mqtt_subscribe() {
  Serial.println("Subscribing");
  client.subscribe(WATCHER_ENABLED);
  client.setCallback(callback);
}

void mqtt_initialize() {
  Serial.print("Connecting to MQTT on ");
  Serial.println(MQTT_SERVER);
  client.setServer(MQTT_SERVER, MQTT_PORT);
  while (!mqtt_check()) {
    Serial.println("Cannot subscribe, retrying in 2 seconds");
    delay(2000);
  }
}

void mqtt_disconnect() {
  Serial.println("Disconnecting from MQTT");
  client.disconnect();
}

void mqtt_publish(const char *topic, const char *payload) {
  client.publish(topic, payload);
}

void mqtt_loop() { client.loop(); }
