#define WATCHER_ENABLED "watcher/enabled"
#define WATCHER_ALARM "watcher/alarm"

void mqtt_initialize();

void mqtt_subscribe();
bool mqtt_check();
void mqtt_publish(const char *topic, const char *payload);

void mqtt_disconnect();
void mqtt_loop();
