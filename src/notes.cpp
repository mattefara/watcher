#include "notes.h"
#include "pins.h"
#include <Arduino.h>

int melody[] = {NOTE_C4, NOTE_G3};
int noteDurations[] = {4, 8};

void ring_alarm() {
  for (int thisNote = 0; thisNote < 8; thisNote++) {
    int noteDuration = 1000 / noteDurations[thisNote % 2 == 0];
    tone(BUZZER, melody[thisNote % 2 == 0], noteDuration);
    int pauseBetweenNotes = noteDuration * 1.30;
    delay(pauseBetweenNotes);
    noTone(BUZZER);
  }
}
