#include <Arduino.h>
#include <state.h>

RTC_DATA_ATTR State current_state = State::INITIAL;

bool blink_enabled = true;
bool on = true;

void update_blink(bool blink) {
  if (!blink) {
    on = true;
  }
  blink_enabled = blink;
}

void update_state(State state) { current_state = state; }

void update_led(int r, int rv, int g, int gv, int b, int bv) {
  if (blink_enabled) {
    on = !on;
  }
  analogWrite(r, rv * on);
  analogWrite(g, gv * on);
  analogWrite(b, bv * on);
}

void update_led_based_on_state(int r, int g, int b) {
  switch (current_state) {
  case State::INITIAL: {
    update_blink(false);
    update_led(r, 0, g, 0, b, 254); // Blue
    break;
  }
  case State::IDLE: {
    update_blink(true);
    update_led(r, 249, g, 115, b, 6); // Orange
    break;
  }
  case State::ARMED: {
    update_blink(true);
    update_led(r, 255, g, 0, b, 0); // Blinking red
    break;
  }
  case State::ERROR: {
    update_blink(false);
    update_led(r, 255, g, 0, b, 0); // Red
    break;
  }
  case State::ALARM: {
    update_blink(false);
    update_led(r, 255, g, 0, b, 0); // Red
    break;
  }
  default:
    update_state(State::ERROR);
    update_blink(false);
    update_led(r, 255, g, 0, b, 0); // Red
    break;
  }
}

bool is_armed() { return current_state == State::ARMED; }

State get_current_state() { return current_state; }
