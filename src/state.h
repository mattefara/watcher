enum State { INITIAL, ERROR, IDLE, ARMED, ALARM };

void update_blink(bool blink);
void update_state(State state);

void update_led(int r, int rv, int g, int gv, int b, int bv);
void update_led_based_on_state(int r, int g, int b);

bool is_armed();

State get_current_state();
