#include "wifi.h"
#include "state.h"
#include <Arduino.h>
#include <stdio.h>

const int MAX_CONNECTION_ATTEMPTS = 30;

void wifi_initialize() {
  Serial.println("Initializing Wifi");
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
}

bool wifi_check() { return WiFi.isConnected(); }

IPAddress wifi_connect(int r, int g, int b) {
  int connection_attempt = 0;
  wifi_initialize();
  while (!wifi_check()) {
    update_led_based_on_state(r, g, b);
    delay(500);
    if (++connection_attempt % MAX_CONNECTION_ATTEMPTS == 0) {
      wifi_initialize();
    }
  }
  State state = get_current_state();
  return WiFi.localIP();
}
